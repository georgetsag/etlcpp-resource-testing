memtest:
	g++ main.cpp -I etl-19.4.0/include/ -g
	valgrind --tool=massif ./a.out --time-unit=ms
	ms_print massif.out*
	rm massif.out*
	rm a.out