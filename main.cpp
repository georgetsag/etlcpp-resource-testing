#include <iostream>
#include <string>
#include <unistd.h>

#include "etl/vector.h"

#define magic_N 25000

using namespace std;

class Sample {
  public:
  Sample(string t){
    tp = t;
  };
  string getType() {
    return tp;
  }
  private:
  string tp;
};

int main() {
  try{
    Sample smpl = Sample("SampleText");
    etl::vector<Sample, magic_N> vec;
    cout << vec.size() << endl;
    for(int i = 0; i < magic_N; i++) {
      usleep(10);
      vec.push_back(smpl);
    }
    cout << vec.size() << endl;
  }catch(...){
    cout << "Catch Exception" << endl;
  }
  return 0;
}